#!/bin/bash
#
# pgsql_backup.sh - Performing a backup from SGDB Postgresql
#
# Site      : gitlab.com/williecadete
# Author     : Willie Cadete <williecadete@gmail.com.br>
#
#  -------------------------------------------------------------
#   This program performs a backup from postgresql database,
#	it can perform the backup with default settings or you can pass arguments
#  -------------------------------------------------------------
#
# History:
#
#    v1.0 2016-12-28, Willie Cadete:
#        - Initial version it performs dump of all bases
#
# License: MIT.

# Variables declaration
HOST="localhost"
PORT="5432"
USER="postgres"
PASSWORD=""
BKP_PATH="$PWD"
TIMESTAMP1="$(date +%Y%m%d%H%M%S)"
TIMESTAMP2="date +%FT%T"
LOG="/var/log/pgsql_backup.log"
DEBUG="0"

# Início do script

HELP="
Use: $(basename "$0") [OPTIONS]

OPTIONS:
  --host	Set a remote host
  --port 	Set a postgresql port
  --user	User of postgres to run the backup
  --pass	Password of user to run the backup
  --path	Location to store the backup
  --log		Path to log file
  --debug	Enable debug mode
  --version	Show version
  --help	Show this help menu
"

# Handling options of command line

while test -n "$1"
do
  case "$1" in
	--host )
		shift
		HOST="$1"
	;;

	--port )
		shift
		PORT="$1"
	;;
	--user )
		shift
		USER="$1"
	;;
	--pass )
		shift
		PASSWORD="$1"
	;;
	--path )
		shift
		BKP_PATH="$1"
	;;
	--log )
		shift
		LOG="$1"
	;;
	--debug )
		DEBUG="1"
	;;
	--version)
		egrep "^#.*v[0-9]" $0 | tail -n1 | awk -F " " '{print$2}'
		exit 0
	;;
	--help )
		echo "$HELP"
		exit 0
	;;
	*)
		echo "Invalid Option: $1"
		exit 1
	;;
  esac
  shift
done

# Definição das funções 

validate() {

# Validate backup's directory
[ ! -d "$BKP_PATH" ]  && mkdir $BKP_PATH && echo "$($TIMESTAMP2) - Backup's directory $BKP_PATH has been created" >> $LOG
return 0
}

# Function to run the dump

dump() {

export PGPASSWORD=$PASSWORD
pg_dumpall -U $USER -h $HOST -p $PORT | gzip > $BKP_PATH/pgsql_backup_${TIMESTAMP1}.gz && echo "$($TIMESTAMP2) - Full backup has been initialized" >> $LOG

}


# Check the backup integrity

check() {

LAST_BKP="$(ls -t "$BKP_PATH" | head -1)"
gzip -t "$BKP_PATH"/"$LAST_BKP"
if [ "$?" -eq 0 ]; then
  LINHA="$(zcat $BKP_PATH/$LAST_BKP | wc -l)"
  if [ "$LINHA" -gt 0 ]; then
    echo "$($TIMESTAMP2) - Postgresql backup has been finished sucessfully" >> $LOG
  else
    echo "$($TIMESTAMP2) - Postgresql backup has been finished failed" >> $LOG
  fi
else
  echo "$($TIMESTAMP2) - Postgresql backup has been finished failed" >> $LOG
fi	

}

# Enable debug mode
debug(){
set -x
}

# Running the functions
test $DEBUG -eq "1" && debug
validate && dump && check
