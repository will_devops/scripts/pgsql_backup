# PGSQL BACKUP

This script performs a backup of the postgresql database, can be run directly or you can pass configuration parameters, 
this program generates a log.

## Using Options

Parameter | Description
----------|------------
--host | Set a remote host
--port | Set a postgresql port
--user | User of postgres to run the backup
--pass | Password of user to run the backup
--path | Location to store the backup
--log | Path to log file
--debug	| Enable debug mode
--version | Show version
--help | Show this help menu  

## Using Examples

        $ ./pgsql_backup.sh

        $ ./pgsql_backup.sh --host 'example.com' --port 1234 --pass 'P4ssw0rd'

        $./pgsql_backup.sh --path '/opt/backups' --log '/var/log/pgsql_backup'  

## Default Settings

**Host** - localhost  
**Port** - 5432  
**User** - Postgres  
**Password** - Senha em branco  
**Backup Path** - Diretório Atual  
**Log Path** - /var/log/pgsql\_backup.log  
**Debug Mode** - Desligado   
